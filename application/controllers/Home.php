<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function Index(){
		$this->Vista(array('vista'=>'Index'));
	}

	public function Objetivos(){
		$this->Vista(array('vista'=>'Objetivos'));
	}

	public function Organigrama(){
		$this->Vista(array('vista'=>'Organigrama'));
	}

	public function Servicios(){
		$this->Vista(array('vista'=>'Servicios'));
	}

	public function Vision(){
		$this->Vista(array('vista'=>'Vision'));
	}

	public function Vista($array){
		$this->load->view('Layout/Header');
		$this->load->view('Static/'.$array['vista'],$array);
		$this->load->view('Layout/Footer');
	}
}
