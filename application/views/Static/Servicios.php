<div class="container services">
	<div class="row contenedor service">
		<div class="col-xs-12">
			<div class="col-xs-1 circulo">
				<span class="fa fa-star"></span>
			</div>
			<div class="col-xs-9 col-sm-4 textop">
				<h2>Económico</h2>
				<p>Asientos reclinables.</p>
				<p>Cinturónde seguridad en cada asiento.</p>
				<p>Música y video.</p>
				<p>Velocidad controlada.</p>
				<p>GPS con monitoreo satelital durante el trayecto.</p>
			</div>
			<div class="col-xs-1 circulo">
				<span class="fa fa-star"></span>
			</div>
			<div class="col-xs-9 col-sm-4 textop">
				<h2>Ejecutivo</h2>
				<p>Asientos reclinables 140º.</p>
				<p>Cinturónde seguridad en cada asiento.</p>
				<p>Música y video.</p>
				<p>Velocidad controlada.</p>
				<p>GPS con monitoreo satelital durante el trayecto.</p>
			</div>
		</div>
		<div class="col-xs-12">
			<div class="col-xs-1 circulo">
				<span class="fa fa-star"></span>
			</div>
			<div class="col-xs-9 col-sm-4 textop">
				<h2>Imperial Dorado</h2>
				<p>Asientos reclinables.</p>
				<p>Cinturón de seguridad en cada asiento.</p>
				<p>Velocidad controlada.</p>
				<p>GPS con monitoreo satelital durante el trayecto.</p>
				<p>Bus Climatizado (Aire acondicionado).</p>
			</div>
			<div class="col-xs-1 circulo">
				<span class="fa fa-star"></span>
			</div>
			<div class="col-xs-9 col-sm-4 textop">
				<h2>Bus Cama</h2>
				<p>Buses computarizados de última generación.</p>
				<p>Bus de 2 pisos.</p>
				<p>Primer nivel Salón Vip con  (12 asientos Vip 160º).</p>
				<p>Segundo nivel asientos de 140º.</p>
				<p>Cinturón de seguridad en todos los asientos.</p>
			</div>
		</div>
		<div class="col-xs-12">
			<div class="col-xs-1 circulo">
				<span class="fa fa-star"></span>
			</div>
			<div class="col-xs-9 col-sm-4 textop">
				<h2>Dorado Vip 160°</h2>
				<p>Buses computarizados de última generación.</p>
				<p>Bus de 2 pisos.</p>
				<p>Cinturónde seguridad en todos los asientos.</p>
				<p>GPS con monitoreo satelital durante el trayecto.</p>
				<p>Cena y desayuno a bordo.</p>
			</div>
			<div class="col-xs-1 circulo">
				<span class="fa fa-star"></span>
			</div>
			<div class="col-xs-9 col-sm-4 textop">
				<h2>Dorado Vip</h2>
				<p>Buses computarizados de última generación.</p>
				<p>48 asientos reclinables.</p>
				<p>Cinturón de seguridad en todos los asientos.</p>
				<p>GPS con monitoreo satelital durante el trayecto.</p>
				<p>Música y video.</p>
			</div>
		</div>
	</div>
</div>