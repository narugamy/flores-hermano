<div class="container">
	<div class="row contenedor">
		<div class="col-xs-1 circulo">
			<span class="fa fa-star"></span>
		</div>
		<p class="col-xs-9 textop">Fundada en el año 1973, por los hermanos Pastor y Raúl FloresChávez, en la ciudad de Tacna, la empresa empezó sus operaciones exclusivamente con el transporte urbano. Con la visión de mercado que sus fundadores tenían, fue muy rápido el crecimiento de la empresa.Durante 1978, la compañía ingresó al transporte interprovincialcubriendo toda la zona Sur de Perú, atendiendo las rutas de losdepartamentos de Tacna, Moquegua y Arequipa.En la década de 1990, Transportes Flores Hnos. realizó unareestructuración del personal administrativo y con la entrada de Sergio Flores Vildoso, hijo y sobrino de los fundadores, se proyectópara desarrollarse más en las rutas del norte de Lima, así latransportadora inició un nuevo camino. Además, amplió sus servicios desde Tacna y Arequipa con destino a la ciudad de Lima e inauguró nuevos mercados</p>
	</div>
</div>