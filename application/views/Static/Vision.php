<div class="container">
	<div class="row contenedor">
		<div class="col-xs-1 circulo">
			<span class="fa fa-star"></span>
		</div>
		<div class="col-xs-9 textop">
			<h2>Vision</h2>
			<p>Constituirse en la empresa líder en el servicio de transporte a nivel nacional y principal socio de nuestros clientes contribuyendo en el desarrollo nacional.</p>
		</div>
		<div class="col-xs-1 circulo">
			<span class="fa fa-star"></span>
		</div>
		<div class="col-xs-9 textop">
			<h2>Mision</h2>
			<p>Brindar el servicio de transporte de carga nacional; conprofesionalismo, gran voluntad, destreza y una gran flota devehículos que aseguren el transporte de su mercadería en forma oportuna, confiable y segura.</p>
		</div>
	</div>
</div>