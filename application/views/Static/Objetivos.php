<div class="container">
	<div class="row contenedor">
		<div class="col-xs-1 circulo">
			<span class="fa fa-star"></span>
		</div>
		<div class="col-xs-9 textop">
			<h2>Objetivos</h2>
				<p>Considerar al cliente como el centro y motivo del negocio</p>
				<p>Mejorar los procesos, sistemas y medios para adelantarnos a las expectativas de nuestros clientes.</p>
				<p>Trabajar en equipo en todas las áreas de la organización, como así también con nuestros clientes y proveedores.</p>
				<p>Capacitar en forma permanente y sostenida a las personas que conforman la organización.</p>
				<p>Identificar las oportunidades de mejoras y prevenir posibles inconvenientes en las distintas áreas laborales.</p>
		</div>
	</div>
</div>