		<div id="base">
			<div class="container padding-vert-30 margin-top-40">
				<div class="row">
					<!-- Sample Menu -->
					<div class="col-md-3 margin-bottom-20">
						<h3 class="margin-bottom-10">Menu Rapido</h3>
						<ul class="menu">
							<li>
								<a class="fa-tasks" href="#">Placerat facer possim</a>
							</li>
							<li>
								<a class="fa-users" href="#">Quam nunc putamus</a>
							</li>
							<li>
								<a class="fa-signal" href="#">Velit esse molestie</a>
							</li>
							<li>
								<a class="fa-coffee" href="#">Nam liber tempor</a>
							</li>
						</ul>
						<div class="clearfix"></div>
					</div>
					<!-- End Sample Menu -->
					<!-- Contact Details -->
					<div class="col-md-3 margin-bottom-20">
						<h3 class="margin-bottom-10">Detalle de Contacto</h3>
						<p>
							<span class="fa-phone">Telephone:</span>(212)888-77-88
							<br>
							<span class="fa-envelope">Email:</span>
							<a href="mailto:info@programofacil.com">info@programofacil.com</a>
							<br>
							<span class="fa-link">Website:</span>
							<a href="http://www.programofacil.com/Flores">http://www.programofacil.com/Flores</a>
						</p>
						<p>The Dunes, Top Road,
							<br>Strandhill,
							<br>Co. Sligo,
							<br>Ireland</p>
					</div>
					<!-- End Thumbs Gallery -->
					<!-- Disclaimer -->
					<div class="col-md-6 margin-bottom-20">
						<h3 class="margin-bottom-10">Resumen</h3>
						<p>Es una empresa de transporte que está surgiendo en el   mercado laboral competitivo haciendo el transporte de carga por todo elterritorio nacional; no somos una empresa de transporte de carga convencional, nos distinguimos por haber alcanzado gran eficiencia y labor en el transporte de nuestros clientes.Nuestra disposición permanente para encontrar soluciones rápidas,estuvo siempre acompañada de una decidida política, equipamiento y tecnología, que permitieron a Transporte Flores, cumplir con lasexpectativas de calidad de nuestros clientes.</p>
						<div class="clearfix"></div>
					</div>
					<!-- End Disclaimer -->
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
		<!-- Footer Menu -->
		<div id="footer">
			<div class="container">
				<div class="row">
					<div id="copyright" class="text-center">
						<p>Copyright &copy; 2016 Learn . All Rights Reserved | Design by <a>Flores</a></p>
					</div>
				</div>
			</div>
		</div>
		<!-- End Footer Menu -->
		<!-- JS -->
		<script type="text/javascript" src="<?=base_url()?>assets/librerias/jquery/js/jquery.min.js" type="text/javascript"></script>
		<script type="text/javascript" src="<?=base_url()?>assets/librerias/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
		<script type="text/javascript" src="<?=base_url()?>assets/js/scripts.js"></script>
		<script type="text/javascript" src="<?=base_url()?>assets/js/jquery.isotope.js" type="text/javascript"></script>
		<!-- Mobile Menu - Slicknav -->
		<script type="text/javascript" src="<?=base_url()?>assets/js/jquery.slicknav.js" type="text/javascript"></script>
		<!-- Animate on Scroll-->
		<script type="text/javascript" src="<?=base_url()?>assets/js/jquery.visible.js" charset="utf-8"></script>
		<!-- Sticky Div <--></-->
		<script type="text/javascript" src="<?=base_url()?>assets/js/jquery.sticky.js" charset="utf-8"></script>
		<!-- Slimbox2-->
		<script type="text/javascript" src="<?=base_url()?>assets/js/slimbox2.js" charset="utf-8"></script>
		<script src="<?=base_url()?>assets/js/modernizr.custom.js" type="text/javascript"></script>
	</body>
</html>