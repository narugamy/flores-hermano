<!DOCTYPE HTML>
<html>
	<head>
		<title>Empresa de Transporte Hermanos Flores</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="keywords" content="Learn Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template,
	Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
		<!-- Custom Theme files -->
		<link href="<?=base_url() ?>assets/librerias/bootstrap/css/bootstrap.min.css" rel='stylesheet' type='text/css' />
		<!----font-Awesome----->
		<link rel="stylesheet" href="<?=base_url() ?>assets/css/font-awesome.css" rel="stylesheet">
		<link rel="stylesheet" href="<?=base_url() ?>assets/css/nexus.css" rel="stylesheet">
		<link rel="stylesheet" href="<?=base_url() ?>assets/css/responsive.css" rel="stylesheet">
		<!-- Google Fonts-->
		<link href="http://fonts.googleapis.com/css?family=Roboto:400,300" rel="stylesheet" type="text/css">
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300" rel="stylesheet" type="text/css">
		<!----font-Awesome----->
	</head>
	<body>
	<div id="body_bg">
		<div id="pre_header" class="container">
			<div class="row margin-top-10 visible-lg">
				<div class="col-md-6">
					<p>
						<strong>Telefono:</strong>&nbsp;1-800-123-4567</p>
				</div>
				<div class="col-md-6 text-right">
					<p>
						<strong>Email:</strong>info@programofacil.com</p>
				</div>
			</div>
		</div>
		<div class="primary-container-group">
			<!-- Background -->
			<div class="primary-container-background">
				<div class="primary-container"></div>
				<div class="clearfix"></div>
			</div>
			<!--End Background -->
			<div class="primary-container">
				<div id="header" class="container">
					<div class="row">
						<!-- Logo -->
						<div class="logo">
							<a href="index.html" title="">
								<h1>Flores Express</h1>
							</a>
						</div>
						<!-- End Logo -->
						<ul class="social-icons pull-right hidden-xs">
							<li class="social-rss">
								<a href="#" target="_blank" title="RSS"></a>
							</li>
							<li class="social-twitter">
								<a href="#" target="_blank" title="Twitter"></a>
							</li>
							<li class="social-facebook">
								<a href="#" target="_blank" title="Facebook"></a>
							</li>
							<li class="social-googleplus">
								<a href="#" target="_blank" title="GooglePlus"></a>
							</li>
						</ul>
					</div>
				</div>
				<!-- Top Menu -->
				<div id="hornav" class="container no-padding">
					<div class="row">
						<div class="col-md-12 no-padding">
							<div class="pull-right visible-lg">
								<ul id="hornavmenu" class="nav navbar-nav">
									<li>
										<a href="<?=base_url()?>" class="fa-home">Home</a>
									</li>
									<li>
										<span class="fa-copy">Paginas</span>
										<ul>
											<li>
												<a href="<?=base_url()?>vision">Vision</a>
											</li>
											<li>
												<a href="<?=base_url()?>objetivos">Objetivos</a>
											</li>
											<li>
												<a href="<?=base_url()?>servicios">Servicios</a>
											</li>
											<li>
												<a href="<?=base_url()?>organigrama">Organigrama</a>
											</li>
										</ul>
									</li>
									<li>
										<span class="fa-th">Servicios</span>
										<ul>
											<li>
												<a href="portfolio-2-column.html">2 Column</a>
											</li>
											<li>
												<a href="portfolio-3-column.html">3 Column</a>
											</li>
											<li>
												<a href="portfolio-4-column.html">4 Column</a>
											</li>
											<li>
												<a href="portfolio-6-column.html">6 Column</a>
											</li>
										</ul>
									</li>
									<li>
										<span class="fa-font">Blog</span>
										<ul>
											<li>
												<a href="blog-list.html">Blog</a>
											</li>
											<li>
												<a href="blog-single.html">Blog Single Item</a>
											</li>
										</ul>
									</li>
									<li>
										<a href="contact.html" class="fa-comment">Contactenos</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>